import React from "react";
import mail from "../assets/icons/mail.svg";
import fb from "../assets/icons/fb.svg";
import ig from "../assets/icons/ig.svg";
import linkedin from "../assets/icons/in.svg";

function Footer() {
  return (
    <>
      {/* <!-- FOOTER --> */}
      <div className="footer">
        <div className="footer_left">
          <div>
            <h5>Contact Us</h5>
          </div>
          <div>
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="mailto:contact@yokesen.com"
            >
              <img src={mail} alt="mail" />
            </a>
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.facebook.com/yokesen.id/"
            >
              <img src={fb} alt="fb" />
            </a>
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.instagram.com/yokesen_id/?hl=en"
            >
              <img src={ig} alt="ig" />
            </a>
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://id.linkedin.com/company/yokesentechnology "
            >
              <img src={linkedin} alt="linkedin" />
            </a>
          </div>
        </div>

        <div className="footer_right">
          <p>Copyright ©Yokesen 2021</p>
        </div>
      </div>
    </>
  );
}

export default Footer;
