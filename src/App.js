import React from "react";
import { Route, Switch } from "react-router-dom";
import Landing from "./pages/Landing";
import Home from "./pages/Home";
import "./App.css";

function App() {
  return (
    <>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/lp/digital-marketing-yokesen" exact component={Landing} />
      </Switch>
    </>
  );
}

export default App;
