import React from "react";
import Footer from "../components/Footer";

import ellipse from "../assets/home/ellipse.png";
import logo from "../assets/home/logo.svg";
import heroDesc from "../assets/home/hero-desc.png";
import heroImg from "../assets/home/hero-img.png";
import searchImg from "../assets/home/search.svg";
import grab from "../assets/home/grab.png";
import gofood from "../assets/home/gofood.png";
import onsite from "../assets/home/onsite.png";
import productImg from "../assets/home/product-img.png";
import productDesc from "../assets/home/product-desc.png";
import product1 from "../assets/home/product/product-1.png";
import product2 from "../assets/home/product/product-2.png";
import product3 from "../assets/home/product/product-3.png";
import product4 from "../assets/home/product/product-4.png";
import product5 from "../assets/home/product/product-5.png";
import product6 from "../assets/home/product/product-6.png";

function Home() {
  return (
    <>
      <div className="home_bg">
        <div className="bg_kopi">
          {/* HOME HERO */}
          <div className="home_hero">
            <img src={ellipse} alt="" className="home_hero_ellipse" />
            <div className="container">
              <div className="header">
                <div className="logo">
                  <img src={logo} alt="" />
                </div>
              </div>
              <div className="row home_hero_content">
                <div className="col-md-6 home_hero_left">
                  <img src={heroDesc} alt="" className="home_hero_desc" />
                  <p>
                    Dibuatkan langsung sesuai pesanan, supaya kamu mendapatkan
                    pengalaman terbaik dalam menikmati rasa segelas kopi
                  </p>
                  <a
                    href="https://www.instagram.com/kopigrontol.id/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <button>Order via DM</button>
                  </a>
                </div>
                <div className="col-md-6 home_hero_right">
                  <img src={heroImg} alt="" className="img-fluid" />
                </div>
              </div>
            </div>
          </div>

          <div className="container">
            {/* HOME CARA PESAN */}
            <div className="section_one_container">
              <h3 className="text-center">Cara Pemesanan</h3>
              <img src={searchImg} alt="seacrh" className="search_img" />

              <div className="row">
                <div className="col-md-4 home_section_one">
                  <img src={grab} alt="" />
                  <p>Order online aja via Grabfood</p>
                </div>
                <div className="col-md-4 home_section_one">
                  <img src={gofood} alt="" />
                  <p>Order online aja via Gofood</p>
                </div>
                <div className="col-md-4 home_section_one">
                  <img src={onsite} alt="" />
                  <p>Order onsite di store Kopi Grontol</p>
                </div>
              </div>
            </div>
            {/* END CARA PESAN */}

            {/* PRODUCT DESC */}
            <div className="row" style={{ marginTop: "50px" }}>
              <div className="col-md-6">
                <img src={productImg} alt="" className="img-fluid" />
              </div>
              <div className="col-md-6 grontol_desc">
                <img src={productDesc} alt="" />
                <p>
                  Kopi Grontol disajikan dengan Kopi Arabika Specialty Single
                  Origin. Salah satu kopi andalan dari Kopi Grontol adalah Kopi
                  Aceh Gayo. Kopi ini memiliki karakter body dan aroma yang
                  kuat. Rasa dari Kopi Aceh Gayo terdapat sedikit rasa rempah
                  dan tingkat keasaman rendah, namun kopi ini tidak terlalu
                  pahit. Kami menyajikan langsung berdasarkan pesanan untuk
                  memastikan pelanggan mendapatkan pengalaman terbaik ketika
                  menikmati Kopi Grontol.
                </p>
                <a
                  href="https://www.instagram.com/kopigrontol.id/"
                  target="_blank"
                  rel="noreferrer"
                >
                  <button>Order via DM</button>
                </a>
              </div>
            </div>
            {/* END PRODUCT DESC */}

            {/* PRODUCT LIST */}
            <div className="product_container">
              <h3>Menu</h3>
              <div className="row">
                <div className="col-md-4 col-6">
                  <div className="effect-image-1">
                    <img src={product1} alt="" className="product_img" />
                    <div className="overlay_text overlay-text-1">
                      <p className="product_desc">
                        Espresso disajikan dingin dengan Fresh Milk premium,
                        palm sugar, dan Durian Ice cream . Dengan pilihan biji
                        kopi Arabika Single Origin
                      </p>
                      <a
                        href="https://www.instagram.com/kopigrontol.id/"
                        target="_blank"
                        rel="noreferrer"
                      >
                        <button>Order via DM</button>
                      </a>
                    </div>
                  </div>
                </div>

                <div className="col-md-4 col-6">
                  <div className="effect-image-1">
                    <img src={product2} alt="" className="product_img" />
                    <div className="overlay_text overlay-text-1">
                      <p className="product_desc">
                        Espresso disajikan dingin dengan es batu, dengan pilihan
                        biji kopi Arabika
                      </p>
                      <a
                        href="https://www.instagram.com/kopigrontol.id/"
                        target="_blank"
                        rel="noreferrer"
                      >
                        <button>Order via DM</button>
                      </a>
                    </div>
                  </div>
                </div>

                <div className="col-md-4 col-6">
                  <div className="effect-image-1">
                    <img src={product3} alt="" className="product_img" />
                    <div className="overlay_text overlay-text-1">
                      <p className="product_desc">
                        Espresso disajikan dingin dengan fresh milk premium dan
                        palm sugar. Dengan pilihan biji kopi Arabika Single
                        Origin
                      </p>
                      <a
                        href="https://www.instagram.com/kopigrontol.id/"
                        target="_blank"
                        rel="noreferrer"
                      >
                        <button>Order via DM</button>
                      </a>
                    </div>
                  </div>
                </div>

                <div className="col-md-4 col-6">
                  <div className="effect-image-1">
                    <img src={product4} alt="" className="product_img" />
                    <div className="overlay_text overlay-text-1">
                      <p className="product_desc">
                        Espresso disajikan dingin dengan Fresh Milk premium,
                        palm sugar, dan Vanilla Ice cream . Dengan pilihan biji
                        kopi Arabika Single Origin
                      </p>
                      <a
                        href="https://www.instagram.com/kopigrontol.id/"
                        target="_blank"
                        rel="noreferrer"
                      >
                        <button>Order via DM</button>
                      </a>
                    </div>
                  </div>
                </div>

                <div className="col-md-4 col-6">
                  <div className="effect-image-1">
                    <img src={product5} alt="" className="product_img" />
                    <div className="overlay_text overlay-text-1">
                      <p className="product_desc">
                        Steamed Fresh Corn with coconuts and white sugar
                      </p>
                      <a
                        href="https://www.instagram.com/kopigrontol.id/"
                        target="_blank"
                        rel="noreferrer"
                      >
                        <button>Order via DM</button>
                      </a>
                    </div>
                  </div>
                </div>

                <div className="col-md-4 col-6">
                  <div className="effect-image-1">
                    <img src={product6} alt="" className="product_img" />
                    <div className="overlay_text overlay-text-1">
                      <p className="product_desc">
                        Espresso disajikan dingin dengan Fresh Milk premium,
                        palm sugar, dan Durian Ice cream . Dengan pilihan biji
                        kopi Arabika Single Origin
                      </p>
                      <a
                        href="https://www.instagram.com/kopigrontol.id/"
                        target="_blank"
                        rel="noreferrer"
                      >
                        <button>Order via DM</button>
                      </a>
                    </div>
                  </div>
                </div>
                {/* =-=-=-=-=-=-=-=-=-=-=-= */}
              </div>
            </div>
            {/* END PRODUCT LIST */}
          </div>
        </div>
        {/* END BG KOPI */}
      </div>

      <Footer />
    </>
  );
}

export default Home;
