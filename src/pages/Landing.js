import React from "react";
import ornament from "../assets/ornament-top.svg";
import logo from "../assets/logo.png";
import logoKopi from "../assets/logo-kopi.png";
import mainImage from "../assets/main_image.png";
import illustration1 from "../assets/illust-1.svg";
import illustration2 from "../assets/illust-2.svg";
import illustration3 from "../assets/illust-3.svg";
import bukti1 from "../assets/bukti1.png";
import bukti2 from "../assets/bukti2.png";
import bukti3 from "../assets/bukti3.png";
import bukti1_icon from "../assets/bukti1-icon.svg";
import bukti2_icon from "../assets/bukti2-icon.svg";
import bukti3_icon from "../assets/bukti3-icon.svg";
import google from "../assets/google.png";
import Footer from "../components/Footer";
import { Link } from "react-router-dom";

function Landing() {
  return (
    <>
      {/* <!-- HERO --> */}
      <div className="hero">
        <div className="hero_container">
          <img src={ornament} alt="ornament" className="hero_ornament" />
          <img src={logoKopi} alt="" className="hero_logo" />
          <div className="from_yokesen">
            FROM <img src={logo} alt="logo" className="hero_logo" />
          </div>
          <img src={mainImage} alt="main_image" className="hero_img" />
          <h2>
            Nah kan, pasti tadi habis ketemu kita di{" "}
            <img src={google} alt="google" className="google_img" />{" "}
          </h2>
          <h1>Mau brand-mu mudah ditemukan seperti ini?</h1>
          <h1>
            <strong>YOKESEN</strong> jawabannya!
          </h1>
          <p>
            Dapatkan <i>maximal exposure</i> seperti Kopi Grontol dan menghemat
            50 persen budgetmu dengan solusi digital Yokesen Teknologi.
          </p>
          <Link to="/">
            <button type="button">
              Cari tahu tentang <br />
              <strong>Kopi Grontol</strong>
            </button>
          </Link>
          <button type="button" className="learn_more">
            Pelajari lebih lanjut di
            <br />
            <strong>Yokesen Teknologi</strong>
          </button>
        </div>
      </div>

      {/* <!-- SECTION 1 --> */}
      <div className="container">
        <div className="section_one text-center">
          <h2>Solusi Bisnis Digital Ala Kopi Grontol</h2>
          <div className="row">
            <div className="col-md-4 section_one_item">
              <img src={illustration1} alt="illustration" />
              <h5>Digital Strategy Management</h5>
              <p>
                Perluas jangkauan pasar melalui <i>funneling strategy</i> untuk
                membangun aset-aset digital yang berguna bagi bisnis anda
              </p>
            </div>

            <div className="col-md-4 section_one_item">
              <img src={illustration3} alt="illustration" />
              <h5>Sales Optimization</h5>
              <p>
                Maksimalkan pesan antar online seperti GrabFood dan GoFood
                dengan <i>marketing optimization</i> strategy
              </p>
            </div>

            <div className="col-md-4 section_one_item">
              <img src={illustration2} alt="illustration" />
              <h5>Growth Hack Strategy</h5>
              <p>
                Memperbesar bisnismu dengan mencarikan calon franchisee atau
                reseller melalui leads generation strategy
              </p>
            </div>
          </div>
        </div>
      </div>

      {/* <!-- SECTION 2 --> */}
      <div className="container-fluid" style={{ position: "relative" }}>
        <div className="section_two">
          <h2 className="text-center">
            Apa yang akan kita lakukan pada bisnis Anda?
          </h2>

          {/* <!-- BUKTI 1 --> */}
          <div className="row">
            <div className="col-md-6 section_two_img">
              <img src={bukti1} alt="" className="img-fluid" />
            </div>
            <div className="col-md-6 section_two_desc">
              <img src={bukti1_icon} alt="" />
              <h1>Membangun funnel marketing</h1>
              <p>
                Wadah untuk mengumpulkan aset-aset digital yang dapat
                dikonversikan menjadi bisnis sehingga mendapatkan{" "}
                <i>Return On Investment</i> yang meningkat
              </p>
            </div>
          </div>

          {/* <!-- BUKTI 2 --> */}
          <div className="row bukti2">
            <div className="col-md-6 section_two_img bukti2_img">
              <img src={bukti2} alt="" className="img-fluid" />
            </div>
            <div className="col-md-6 section_two_desc bukti2_desc">
              <img src={bukti2_icon} alt="" />
              <h1>Winning the Market</h1>
              <p>
                Bekerjasama dengan Anda untuk membantu calon customer memahami
                produk dan brand sehingga dapat mempercepat pengambilan
                keputusan untuk melakukan pembelian
              </p>
            </div>
          </div>

          {/* <!-- BUKTI 3 --> */}
          <div className="row">
            <div className="col-md-6 section_two_img">
              <img src={bukti3} alt="" className="img-fluid" />
            </div>
            <div className="col-md-6 section_two_desc">
              <img src={bukti3_icon} alt="" />
              <h1>Hyper Targeting Lead Generation</h1>
              <p>
                Membangun posisi brand yang benar melalui placement dan
                pemilihan audiens yang tepat sehingga dapat menjaring
                calon-calon mitra bisnis yang menjanjikan
              </p>
            </div>
          </div>
        </div>
        <img src={ornament} alt="ornament" className="hero_ornament_bottom" />
      </div>
      <Footer />
    </>
  );
}

export default Landing;
